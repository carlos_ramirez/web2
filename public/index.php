<?php
// File: /public/home.php
require __DIR__ . '/../vendor/autoload.php';// Se inserta el autoload de composer

// variables de entorno
$dotenv = Dotenv\Dotenv::createImmutable('../');
$dotenv->load();
$GLOBALS["DOTENV"] = $dotenv->load();

error_reporting(E_ALL ^ E_NOTICE);
$configuration = [
    'settings' => [
        'displayErrorDetails' => $GLOBALS["DOTENV"]['APP_DEBUG'] == 'true',
    ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);// Se instancia la aplicación con la configuración del archivo app.php

// Se inserta las diferentes configuraciones
require __DIR__ . '/../config/dependencies.php';
require __DIR__ . '/../config/routes.php';

// Se arranca la aplicacion
$app->run();
