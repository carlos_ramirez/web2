<?php

function seedPassword(): string{
    return 'p_web_2';
}

function base_path():string
{
    return ($GLOBALS["DOTENV"]['APP_ENV']==='dev') ? '/web2/public' : '';
}

function base_path_form($urn):string
{
    return 'http://'.$_SERVER["HTTP_HOST"].base_path().'/'.$urn;
}

function patternPassword($password){
    if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z!@#$%]{8,16}$/', $password)) {
        return true;
    }
    return false;
}





