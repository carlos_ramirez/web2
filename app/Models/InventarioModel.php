<?php


namespace MiWeb\Models;


use Envms\FluentPDO\Exception;
use MiWeb\Lib\Database;
use MiWeb\Traits\SessionTrait;

class InventarioModel
{
    use SessionTrait;
    private $fluent;
    private $auth;
    private $idUser;

    public function __CONSTRUCT()
    {
        //Atraemos el objeto de la conexion a la base de datos
        //Verificamos si el usuario esta logueado
        $this->fluent = DataBase::StartUp();
        $this->auth   = $this->dataSession();
        $this->idUser = (int)$this->auth[0]['id'];
    }

    public function select():array
    {
        try
        {
            $query = $this->fluent->from('inventario')->fetchAll();
            $vector=[];
            foreach($query as $row)
            {
                $vector[]=[
                    'id'                => $row->id,
                    'id_producto'       => $row->id_producto,
                    'nombre_producto'   => $row->nombre_producto,
                    'descripcion'       => $row->descripcion,
                    'costo'             => $row->costo,
                    'precio'            => $row->precio,
                    'existencia'        => $row->existencia,
                    'created_at'        => $row->created_at,
                    'updated_at'        => $row->updated_at,
                ];
            }
            return $vector;

        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
    public function insert($data)
    {
        try
        {
            $this->fluent->insertInto('inventario', $data)->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function update($data)
    {
        try
        {
            $id = $data['id'];
            unset($data['id']);
            $this->fluent->update('inventario', $data, $id)->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function delete($id)
    {
        try
        {
            $this->fluent->deleteFrom('inventario', $id)
                ->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }


}
