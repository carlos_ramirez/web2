<?php


namespace MiWeb\Models;

use Carbon\Carbon;
use Envms\FluentPDO\Exception;
use MiWeb\Lib\Database;
use MiWeb\Traits\SessionTrait;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserModel
{
    use SessionTrait;
    private $fluent;

    public function __CONSTRUCT()
    {
        //Atraemos el objeto de la conexion a la base de datos
        $this->fluent = DataBase::StartUp();
    }

    public function index():array
    {
        try
        {
            $query = $this->fluent->from('usuarios')->fetchAll();
            $vector=[];
            foreach($query as $row)
            {
                $vector[]=[
                    'id_usuario' => $row->id_usuarios
                ];
            }
            return $vector;
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function login($id_usuarios,$password)
    {
        if(self::verifyIdUsuario($id_usuarios))
        {
            if(self::verifyMachIdUsuarioVsPassword($id_usuarios,$password))
            {
                $id = $this->fluent->from('usuarios')->where('id_usuario',$id_usuarios)->fetch()->id;
                $this->createSession($id);
            }
        }
        return (self::verifyMachIdUsuarioVsPassword($id_usuarios,$password))
            ? ['login'=>true,'id'=>$id]
            : ['login'=>false,'id'=>$id];
    }

    public function delete($id)
    {
        try
        {
            $this->fluent->deleteFrom('usuarios', $id)
                ->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function Actualizar($data)
    {
        try
        {
            $id = $data['id'];
            unset($data['id']);
            $this->fluent->update('usuarios', $data, $id)
                ->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function register($data)
    {
        try
        {
            $this->fluent->insertInto('usuarios', $data)->execute();
        }
        catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function verifyIdUsuario($id_usuarios):bool
    {
        try {
            $count = $this->fluent->from('usuarios')->where('id_usuario',$id_usuarios)->count();
            return ($count > 0) ?? true;
        }catch (Exception $e)
        {
            die($e->getMessage());
        }

    }

    private function verifyMachIdUsuarioVsPassword($id_usuarios,$password):bool
    {
        try {
            $count = $this->fluent->from('usuarios')
                ->where('id_usuario',$id_usuarios)
                ->where('password',hash('SHA256',$password))
                ->count();
            if( $count == 1 )
            {
                $id = $this->fluent->from('usuarios')
                    ->where('id_usuario',$id_usuarios)
                    ->where('password',$password)
                    ->fetch();
                return true;
            }
            return false;
        }catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function verifySessionActive($SSID):bool
    {
        try {
            if(isset($_COOKIE['PHPSESSID'])  && $_COOKIE['PHPSESSID'] !== ''){
                $count = $this->fluent->from('usuarios')
                    ->where('session_id',$SSID)
                    ->count();
                return $count > 0;
            }else{
                return false;
            }

        }catch (Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function getDataUserActive($SSID){
        try
        {
            $query = $this->fluent->from('usuarios')->where('session_id',$SSID)->fetch();

            /*
            $date = new Carbon($query->session_at);
            $diff = $date->diffInMinutes(Carbon::now('America/Mexico_City'));

            if( $diff > 5 ){
                self::closeSession();
            }
*/
            return $query;
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }

    public function checkSession($SSID){
        if(self::verifySessionActive($SSID)){
            return self::getDataUserActive($SSID);
        }else{
            return null;
        }
    }

    public function closeSession()
    {
        $set =
            [
            'session_id'    =>  null,
            'session_at'    =>  Carbon::now('America/Mexico_City'),
            'updated_at'    =>  Carbon::now('America/Mexico_City'),
            ];
        $this->fluent->update('usuarios', $set, $GLOBALS['ID'])->execute();
    }
}
