<?php
// File: /app/Controllers/Controller.php

namespace MiWeb\Controllers;

class Controller
{

    /** @var \League\Plates\Engine Acceso al motor de plantillas */
    protected $view;

    /**
     * Usando inyección de dependencia se le pasa al constructor
     * el contenedor de la aplicación. Así se dispone de las
     * dependencias establecidas en config/dependencies.php.
     *
     * @param $container \Pimple\Container Contenedor de la aplicación
     */
    public function __construct($container)
    {
        $this->view = $container->get('templates');
    }

}
