<?php


namespace MiWeb\Controllers\Erp;

use Carbon\Carbon;
use MiWeb\Controllers\Controller;
use MiWeb\Models\InventarioModel;
use MiWeb\Traits\SessionTrait;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class InventarioController extends Controller
{
    use SessionTrait;
    private $auth;
    private $idUser;

    public function __construct($container)
    {
        parent::__construct($container);
        $this->auth   = $this->dataSession();
        $this->idUser = (int)$this->auth[0]['id'];
    }

    public function home(Request $request, Response $response, $args)
    {
        $data     = new InventarioModel();
        $response = $data->select();
        return $this->view->render('Erp/home',compact('response'));
    }

    public function insert(Request $request, Response $response, $args)
    {
        $agenda = new InventarioModel();
        $agenda->insert([
            'id_producto'       => $request->getParam('id_producto'),
            'nombre_producto'   => $request->getParam('nombre_producto'),
            'descripcion'       => $request->getParam('descripcion'),
            'costo'             => $request->getParam('costo'),
            'precio'            => $request->getParam('precio'),
            'existencia'        => $request->getParam('existencia'),
            'created_at'        =>  Carbon::now('America/Mexico_City'),
            'updated_at'        =>  Carbon::now('America/Mexico_City'),
            ]);
        return $response->withStatus(302)->withRedirect(base_path().'/home');
    }

    public function update(Request $request, Response $response, $args)
    {
        $agenda = new InventarioModel();
        $agenda->update([
            'id'                =>  $request->getParam('id'),
            'nombre_producto'   => $request->getParam('nombre_producto'),
            'descripcion'       => $request->getParam('descripcion'),
            'costo'             => $request->getParam('costo'),
            'precio'            => $request->getParam('precio'),
            'existencia'        => $request->getParam('existencia'),
            'updated_at'        =>  Carbon::now('America/Mexico_City'),
        ]);
        return $response->withStatus(302)->withRedirect(base_path().'/home');
    }

    public function delete(Request $request, Response $response, $args)
    {
        $agenda = new InventarioModel();
        $agenda->delete($request->getParam('id'));
        return $response->withStatus(302)->withRedirect(base_path().'/home');
    }

}
