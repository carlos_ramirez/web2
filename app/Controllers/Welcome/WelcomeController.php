<?php


namespace MiWeb\Controllers\Welcome;


use Carbon\Carbon;
use MiWeb\Controllers\Controller;
use MiWeb\Models\UserModel;
use MiWeb\Traits\SessionTrait;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class WelcomeController extends Controller
{
    /**
     * Usando inyección de dependencia se le pasa al constructor
     * el contenedor de la aplicación. Así se dispone de las
     * dependencias establecidas en config/dependencies.php.
     *
     * @param $container \Pimple\Container Contenedor de la aplicación
     */
    use SessionTrait;

    public function __construct($container)
    {
        parent::__construct($container);
    }

    /**
     * Acción única de la aplicación que demuestra el paso de argumentos
     * a la platilla.
     *
     * @param $request \Psr\Http\Message\ServerRequestInterface Petición a la
     * que servir.
     * @param $response \Psr\Http\Message\ResponseInterface Respuesta a la
     * petición.
     * @param $array array Contiene cada uno de los parámetros de la petición
     *
     * @return string con la plantilla ya renderizada.
     */
    public function index(Request $request, Response $response, $args)
    {
        $users = new UserModel();
        $nombre = $args['nombre'] ?? 'mundo';
        $listUser = $users->index();
        return ($this->statusSession()['bool'])
            ? $response->withStatus(302)->withRedirect( base_path().'/home')
            : $this->view->render('Welcome/index', compact('nombre','listUser'));

    }

    public function login(Request $request, Response $response, $args)
    {
        $users = new UserModel();
        $nombre = $args['nombre'] ?? 'mundo';
        $listUser = $users->index();
        return ($this->statusSession()['bool'])
            ? $response->withStatus(302)->withRedirect( base_path().'/home')
            : $this->view->render('Welcome/login', compact('nombre','listUser'));
    }

    public function loginUser(Request $request, Response $response, $args)
    {
        $users = new UserModel();
        $array = $users->login($request->getParam('id_usuario'),$request->getParam('password'));
        if($array['login'])
        {
            return $response->withStatus(200)->withRedirect( base_path().'/home');
        }else{
            return $response->withStatus(302)->withRedirect(base_path().'/login?msg='.hash('SHA256','error|credenciales_invalidas'));
        }

    }

    public function register(Request $request, Response $response, $args)
    {
        return ($this->statusSession()['bool'])
            ? $response->withStatus(302)->withRedirect( base_path().'/home')
            : $this->view->render('Welcome/register');
    }

    public function registerUser(Request $request, Response $response, $args)
    {
        $data = self::reCaptchaV3($request);
        if($data["data"]["json"]["success"] == '1' && $data["data"]["json"]["action"] == $data["data"]["action"] && $data["data"]["json"]["score"] >= 0.5) {
            $return = self::registerUserInsert($request,$response);
            return $response->withStatus($return['code'])->withRedirect(base_path().'/register?msg='.hash('SHA256',$return['msg']));
        } else {
            return $response->withStatus(302)->withRedirect(base_path().'/register?msg='.hash('SHA256','error|reCaptCha'));
        }
    }

    private function reCaptchaV3($request){
        // Valida el reCaptchaV3
        $recaptcha_secret = $GLOBALS["DOTENV"]['RE_CAPTCHA_SECRET'];
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $token = $request->getParam('token');
        $action = $request->getParam('action');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => $recaptcha_secret, 'response' => $token)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return ["data"=>["json"=>json_decode($response, true), "action"=>$action]];
    }

    private function registerUserInsert($request,$response){
        $users = new UserModel();
        if(patternPassword($request->getParam('password'))){// Valida el password
            return ['bool'=>false, 'code'=>302, 'msg'=>'error|password'];
        }elseif($users->verifyIdUsuario($request->getParam('id_usuario'))) {// Valida si el ID del usuario esta duplicado
            return ['bool'=>false, 'code'=>302, 'msg'=>'error|existe_usuario'];
        }else{// Inserta el dato y devuelve un estatus 200
            $users->register([
                'id_usuario'        =>  $request->getParam('id_usuario'),
                'password'          =>  hash('SHA256',$request->getParam('password')),
                'nombre_usuario'    =>  $request->getParam('nombre_usuario'),
                'tipo_usuario'      =>  $request->getParam('tipo_usuario'),
                'created_at'        =>  Carbon::now('America/Mexico_City'),
                'updated_at'        =>  Carbon::now('America/Mexico_City'),
            ]);
            return ['bool'=>true, 'code'=>302, 'msg'=>'success|registro_exitoso'];
        }

    }

    public function logout(Request $request, Response $response, $args)
    {
        $users = new UserModel();
        $this->sessionDestroy();
        $users->closeSession();
        return $response->withStatus(302)->withRedirect(base_path().'/');
    }

}
