<?php
namespace MiWeb\Lib;

use Envms\FluentPDO\Exception;
use PDO;

class Database
{
    public static function StartUp()
    {

        try
        {
            $pdo = new PDO('mysql:host=localhost;dbname=p_web_2;charset=utf8', $GLOBALS["DOTENV"]['DB_USERNAME'], $GLOBALS["DOTENV"]['DB_PASSWORD']);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);

           return new \Envms\FluentPDO\Query($pdo);
        }
        catch(Exception $e)
        {
            die($e->getMessage());
        }
    }
}
