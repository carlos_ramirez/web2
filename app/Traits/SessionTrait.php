<?php


namespace MiWeb\Traits;


use Carbon\Carbon;
use Envms\FluentPDO\Exception;
use MiWeb\Lib\Database;

trait SessionTrait
{
    public function createSession($idUser)
    {
        self::sessionStart();
        self::updateSessionForUser($idUser, session_id());
    }

    public function statusSession(): array
    {
        if (isset($_SESSION["login"])) {
            return [
                'bool' => true,
                'string' => $_SESSION["login"],
            ];
        }
        return [
            'bool' => false,
            'string' => '',
        ];

    }

    public function dataSession(): array
    {
        if (self::statusSession()['bool']) {
            $db = DataBase::StartUp();
            $collection = $db->from('usuarios')->where('session_id', self::statusSession()['string'])->fetchAll();
            $array = [];
            foreach ($collection as $data) {
                $array[] = [
                    'id' => $data->id,
                    'id_usuario' => $data->id_usuario,
                    'nombre_usuario' => $data->nombre_usuario,
                    'tipo_usuario' => $data->tipo_usuario,
                    'session_at' => $data->session_at,
                ];
            }
            return $array;
        }
        return [];
    }

    public function sessionDestroy(): void
    {
        session_start();
        session_unset();
        session_destroy();
        session_write_close();
    }

    private function sessionStart(): void
    {
        session_start();
    }

    private function updateSessionForUser($idUser, $idSession)
    {
        $db = DataBase::StartUp();
        try {
            $data = [
                'session_id' => $idSession,
                'session_at' => Carbon::now('America/Mexico_City'),
                'updated_at' => Carbon::now('America/Mexico_City'),
            ];
            $db->update('usuarios', $data, $idUser)
                ->execute();
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
