<?php $this->layout('master') ?>
<p>Home | Bienvenido usuario:
    <strong>
        <?php echo $GLOBALS['NOMBRE_USUARIO'];?>
    </strong>
        | te has logueado como tipo:
    <strong>
        <?php echo $GLOBALS['TIPO_USUARIO']?>
    </strong>
    </p>
<h1 style="text-align: center">Sistema de inventario</h1>
<div style="text-align: center">
    <ul class="nav" style="padding-left: 75%">
        <li class="nav-item"><a href="<?php echo base_path_form('home') ?>"> | Inicio | </a></li>
        <li class="nav-item"><a href="<?php echo base_path_form('logout') ?>"> | Cerrar sesion | </a></li>
</div><hr>
<div class="divCenter">
    <?php if($GLOBALS['TIPO_USUARIO'] == 'A'){ ?>

        <div class="jumbotron">
            <h1 class="display-4">Catalogo de productos</h1>
        </div>
        <?php if($GLOBALS['TIPO_USUARIO'] == 'A'){ ?>

            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalCreate2">
                Nuevo registro
            </button>
            <div class="modal fade"  id="modalCreate2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nuevo producto </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form autocomplete='off' method='post' action='<?php echo base_path_form('insert') ?>'>
                                <input type='hidden' id='text' name='id'/>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Id producto</label>
                                    <input type="number" class="form-control" name='id_producto' placeholder='id producto' required />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Nombre producto</label>
                                    <input type="text" class="form-control" name='nombre_producto' placeholder='nombre_producto' required />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Descripcion</label>
                                    <input type="text" class="form-control" name='descripcion' placeholder='descripcion' required />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Costo</label>
                                    <input type="number" class="form-control" step="0.01" name='costo' placeholder='costo' required />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Precio</label>
                                    <input type="number" class="form-control" step="0.01" name='precio' placeholder='precio' required />
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Existencia</label>
                                    <input type="number" class="form-control" name='existencia'  min="1" pattern="^[0-9]+"  placeholder='existencia' required />
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <table class="table table-sm">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Costo</th>
                <th>Precio</th>
                <th>Existencia</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tboby>
            <?php foreach($response as $data){?>
            <tr>
                <td><?php echo $data['id_producto'] ?></td>
                <td><?php echo $data['nombre_producto'] ?></td>
                <td><?php echo $data['descripcion'] ?></td>
                <td><?php echo $data['costo'] ?></td>
                <td><?php echo $data['precio'] ?></td>
                <td><?php echo $data['existencia'] ?></td>
                <td>
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalEdit2<?php echo $data['id'] ?>">
                        editar
                    </button>
                    <div class="modal fade"  id="modalEdit2<?php echo $data['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Editar producto </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form autocomplete='off' method='post' action='<?php echo base_path_form('update') ?>'>
                                        <input type='hidden' id='text' name='id' value='<?php echo $data['id'] ?>'/>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nombre producto</label>
                                            <input type="text" class="form-control" name='nombre_producto' placeholder='nombre_producto' value="<?php echo $data['nombre_producto'] ?>" required />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Descripcion</label>
                                            <input type="text" class="form-control" name='descripcion' value="<?php echo $data['descripcion'] ?>" placeholder='descripcion' required />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Costo</label>
                                            <input type="number" class="form-control" step="0.01" name='costo' value="<?php echo $data['costo'] ?>" placeholder='costo' required />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Precio</label>
                                            <input type="number" class="form-control" step="0.01" name='precio' value="<?php echo $data['precio'] ?>" placeholder='precio' required />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Existencia</label>
                                            <input type="number" class="form-control" name='existencia'  value="<?php echo $data['existencia'] ?>" min="1" pattern="^[0-9]+"  placeholder='existencia' required />
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <button type="button" class="btn btn-link" data-toggle="modal" data-target="#modalDelete2<?php echo $data['id'] ?>">
                        eliminar
                    </button>
                    <div class="modal fade"  id="modalDelete2<?php echo $data['id'] ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Borrar producto </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form autocomplete='off' method='post' action='<?php echo base_path_form('delete') ?>'>
                                        <input type='hidden' id='text' name='id' value='<?php echo $data['id'] ?>'/>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Nombre producto</label>
                                            <input type="text" class="form-control"  value="<?php echo $data['nombre_producto'] ?>" disabled />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Descripcion</label>
                                            <input type="text" class="form-control"  value="<?php echo $data['descripcion'] ?>" placeholder='descripcion' disabled />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Costo</label>
                                            <input type="number" class="form-control" step="0.01"  value="<?php echo $data['costo'] ?>" placeholder='costo' disabled />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Precio</label>
                                            <input type="number" class="form-control" step="0.01"  value="<?php echo $data['precio'] ?>" placeholder='precio' disabled />
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Existencia</label>
                                            <input type="number" class="form-control"   value="<?php echo $data['existencia'] ?>" min="1" pattern="^[0-9]+"  disabled />
                                        </div>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <?php } ?>
        </tboby>
    </table>
    <?php } ?>

    <?php if($GLOBALS['TIPO_USUARIO'] == 'C'){ ?>
        <div class="jumbotron">
            <h1 class="display-4">Catalogo de productos</h1>
        </div>
        <table class="table table-sm">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Precio</th>
            </tr>
            </thead>
            <tboby>
                <?php foreach($response as $data){?>
                    <tr>
                        <td><?php echo $data['nombre_producto'] ?></td>
                        <td><?php echo $data['descripcion'] ?></td>
                        <td><?php echo $data['precio'] ?></td>
                    </tr>
                <?php } ?>
            </tboby>
        </table>
    <?php } ?>
</div>

