<!-- File: /app/Views/Welcome/index.php -->

<?php $this->layout('master') ?>

<div class="container">
    <ul class="nav" style="padding-left: 75%">
        <li class="nav-item" style="padding: 5%">
            <a class="nav-link active" href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/" >Inicio</a>
        </li>
        <li class="nav-item" style="padding: 5%">
            <a class="nav-link" href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/register" >Registro</a>
        </li>
    </ul>
    <div class="row justify-content-md-center">
        <div class="col-12 col-sm-6 col-md-6">
            <form  autocomplete="new-password" method="post" action="http://<?php echo $_SERVER["HTTP_HOST"].base_path() ?>/loginUser" onsubmit="return val()">
                <h2>Login</h2>
                <?php
                if( isset($_GET['msg']) && $_GET['msg']==hash('SHA256','error|credenciales_invalidas'))
                {
                    echo '<hr><div style="color: brown">No existe ID de usuario </div><hr>';
                }
                ?>
                <div class="form-group">
                    <label>ID de usuario</label>
                    <input class="form-control" name="id_usuario" type="text" id="text" autocomplete="new-password" placeholder="ID de usuario.."  required/>
                </div>
                <div class="form-group">
                    <label>ID de usuario</label>
                    <input class="form-control" name="password" type="password" id="text" autocomplete="new-password"
                           placeholder="password.."
                           maxlength="30"
                           pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\w+))(?![.\n])(?=.*[a-z]).*$"
                           required autofocus/>
                </div>
                <div class="form-group" style="padding-left: 75%"><!--<p>progweb2#</p>-->
                    <button id="enviar" name="enviar" type="submit" class="btn">enviar...</button>
                </div>
            </form>
        </div>
    </div>
</div>

