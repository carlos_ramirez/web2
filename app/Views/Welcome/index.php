<!-- File: /app/Views/Welcome/index.php -->

<?php $this->layout('master') ?>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#" style="padding-right: 75%">Inicio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" style="padding-right: 5%"><a href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/">Inicio</a></li>
            <li class="nav-item" style="padding-right: 5%"><a href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/login">Login</a></li>
            <li class="nav-item" style="padding-right: 5%"><a href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/register">Registro</a></li>
        </ul>
    </div>
</nav>

<h1 style="padding-top: 5%; text-align: center">Importadora SA de CV</h1>
<!--Carousel Wrapper-->
<div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

    <!--Controls-->
    <div class="controls-top">
        <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
        <a class="btn-floating" href="#multi-item-example" data-slide="next"><i
                    class="fas fa-chevron-right"></i></a>
    </div>
    <!--/.Controls-->

    <!--Indicators-->
    <ol class="carousel-indicators">
        <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
        <li data-target="#multi-item-example" data-slide-to="1"></li>
        <li data-target="#multi-item-example" data-slide-to="2"></li>
    </ol>
    <!--/.Indicators-->

    <!--Slides-->
    <div class="carousel-inner" role="listbox">

        <!--First slide-->
        <div class="carousel-item active">
            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdYAxlIl_VzHJdeZqB4pecsPyKCBENdSyFbyNHDIv9ub7Ndxrz2OiRKA9oSics1_mTc2Y&usqp=CAU"
                         alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">Eficiencia</h4>
                        <p class="card-text">Bucamos la mayor eficiencia en entragas consolidadas</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwlootGJ9RozsLO_vn3d_c2Q-ZZdgqWImVCMR5gCsAEa5uRR1BFr3d0Pjwt2CZaMT0O9Q&usqp=CAU"
                         alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">Seguridad</h4>
                        <p class="card-text">Si la mercancia lo require, proveemos seguridad privada y custodios</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="http://www.automotores-rev.com/wp-content/uploads/2019/08/antp-revista-auto-motores-660x330.jpeg"
                         alt="Card image cap">
                    <div class="card-body">
                        <h4 class="card-title">Costo</h4>
                        <p class="card-text">El mejor costo, en el mejor tiempo</p>
                    </div>
                </div>
            </div>

        </div>
        <!--/.First slide-->

        <!--Second slide-->
        <div class="carousel-item">
            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdYAxlIl_VzHJdeZqB4pecsPyKCBENdSyFbyNHDIv9ub7Ndxrz2OiRKA9oSics1_mTc2Y&usqp=CAU">
                    <div class="card-body">
                        <h4 class="card-title">Eficiencia</h4>
                        <p class="card-text">Bucamos la mayor eficiencia en entragas consolidadas</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwlootGJ9RozsLO_vn3d_c2Q-ZZdgqWImVCMR5gCsAEa5uRR1BFr3d0Pjwt2CZaMT0O9Q&usqp=CAU">
                    <div class="card-body">
                        <h4 class="card-title">Seguridad</h4>
                        <p class="card-text">Si la mercancia lo require, proveemos seguridad privada y custodios</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="http://www.automotores-rev.com/wp-content/uploads/2019/08/antp-revista-auto-motores-660x330.jpeg">
                    <div class="card-body">
                        <h4 class="card-title">Costo</h4>
                        <p class="card-text">El mejor costo, en el mejor tiempo</p>
                    </div>
                </div>
            </div>

        </div>

        <div class="carousel-item">
            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTdYAxlIl_VzHJdeZqB4pecsPyKCBENdSyFbyNHDIv9ub7Ndxrz2OiRKA9oSics1_mTc2Y&usqp=CAU">
                    <div class="card-body">
                        <h4 class="card-title">Eficiencia</h4>
                        <p class="card-text">Bucamos la mayor eficiencia en entragas consolidadas</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwlootGJ9RozsLO_vn3d_c2Q-ZZdgqWImVCMR5gCsAEa5uRR1BFr3d0Pjwt2CZaMT0O9Q&usqp=CAU">
                    <div class="card-body">
                        <h4 class="card-title">Seguridad</h4>
                        <p class="card-text">Si la mercancia lo require, proveemos seguridad privada y custodios</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="card mb-2">
                    <img class="card-img-top"
                         src="http://www.automotores-rev.com/wp-content/uploads/2019/08/antp-revista-auto-motores-660x330.jpeg">
                    <div class="card-body">
                        <h4 class="card-title">Costo</h4>
                        <p class="card-text">El mejor costo, en el mejor tiempo</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--/.Slides-->
</div>
<!--/.Carousel Wrapper-->





