<!-- File: /app/Views/Welcome/register.php -->

<?php $this->layout('master') ?>
<script>
    function onSubmit(token) {
        document.getElementById("demo-form").submit();
    }
</script>
<div class="container">
    <ul class="nav" style="padding-left: 75%">
        <li class="nav-item" style="padding: 5%">
            <a class="nav-link active" href="http://<?php  echo $_SERVER["HTTP_HOST"].base_path() ?>/" >Inicio</a>
        </li>
        <li class="nav-item" style="padding: 5%">
            <a class="nav-link" href="http://<?php echo $_SERVER["HTTP_HOST"].base_path() ?>/login">Login</a>
        </li>
    </ul>
    <div class="row justify-content-md-center">
        <div class="col-12 col-sm-6 col-md-6">
            <form id="newsletterForm" autocomplete="new-password"  method="post" action="http://<?php echo $_SERVER["HTTP_HOST"].base_path()?>/registerUser">
                <h2>Registro</h2>
                <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                <?php
                if( isset($_GET['msg']) && $_GET['msg']==hash('SHA256','error|existe_usuario'))
                {
                    echo '<hr><div style="color: brown"> EL usuario ya existe </div><hr>';
                }
                elseif(isset($_GET['msg']) && $_GET['msg']==hash('SHA256','success|registro_exitoso'))
                {
                    echo '<hr><div style="color: blue"> Usuario creado con exito </div><hr>';
                }
                elseif(isset($_GET['msg']) && $_GET['msg']==hash('SHA256','error|password'))
                {
                    echo '<hr><div style="color: brown"> Contraseña erronea (min-max 8/16 dig, 1 min, 1 may, 1 caracter) </div><hr>';
                }
                elseif(isset($_GET['msg']) && $_GET['msg']==hash('SHA256','error|reCaptCha'))
                {
                    echo '<hr><div style="color: brown"> ERROR: Spam </div><hr>';
                }
                ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input class="form-control" id="nombre_usuario" name="nombre_usuario" type="text" autocomplete="new-password" placeholder="nombre.."  required/>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">ID usuario</label>
                    <input class="form-control" id="id_usuario" name="id_usuario" type="number" autocomplete="new-password" placeholder="id_usuario.."  required/>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input class="form-control" id="password" name="password" type="password" autocomplete="new-password" placeholder="password.."  required
                           minlength="8"
                           maxlength="30"
                           autofocus/>
                </div>
                <p><small>Contraseña (min-max 8/16 dig, 1 min, 1 may, 1 caracter)</small></p>
                <hr>
                <label>Tipo de usuario</label>
                <select id="tipo_usuario" name="tipo_usuario" class="form-control" required>
                    <option value="C">C - Cliente</option>
                </select>

                <hr>
                <div class="form-group" style="padding-left: 75%">
                    <input type="submit" value="submit" class="form-control">
                </div>
            </form>
            <script>
                $('#newsletterForm').submit(function(event) {
                    event.preventDefault();
                    var nombre_usuario = $('#nombre_usuario').val();
                    var id_usuario = $('#id_usuario').val();
                    var password = $('#password').val();
                    var tipo_usuario = $('#tipo_usuario').val();

                    grecaptcha.ready(function() {
                        grecaptcha.execute('6LeC9BkcAAAAADckiaPm0pBmT3axUzbaqwNaZvTm', {action: 'subscribe_newsletter'}).then(function(token) {
                            $('#newsletterForm').prepend('<input type="hidden" name="token" value="' + token + '">');
                            $('#newsletterForm').prepend('<input type="hidden" name="action" value="subscribe_newsletter">');
                            $('#newsletterForm').unbind('submit').submit();
                        });;
                    });
                });
            </script>
        </div>
    </div>
</div><!--pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\w+))(?![.\n])(?=.*[a-z]).*$"-->
