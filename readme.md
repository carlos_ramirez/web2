
## Desarrollo Web en PHP 
- Este proyecto ha sido creado para cumplir los requerimentos de la materia Programacion Web Dos de la UnadM.
- Grupo: DS-DPW"-2102-B1-001
- Alumno: Carlos Alberto Ramirez Alfonso
- Clave: ES172007840


## Funcionalidad
- El sistema cuenta con login y registro de usuarios
- Contiene un CRUD que simula un inventario

## Configuracion
- Descargar desde el repositorio https://gitlab.com/carlos_ramirez/web2
- Ejecutar composer update para descargar las paqueterias

## Estructura

Base:
- PHP V-7.4
- HTML5
- CSS puro
- Javascript puro
- Composer como manejador de dependencias
- Rutas basadas en el estandar PSR-4
- Driver PDO de PHP para conexión a la base de datos

Base de datos:
- MySQL

Patrones y metodologias utilizadas
- Programacion orientada a objetos
- Patron MVC
- Principios SOLID

Librerias BackEnd
- Libreria Slim V3 para crear rutas amigables
- Libreria Carbon V2 para el manejo de fechas
- Librearia Fluent para el mapeo de consultas a la base de datos




