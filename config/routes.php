<?php


use MiWeb\Models\UserModel;

$users = new UserModel();
$sessionActive = $users->verifySessionActive($_COOKIE["PHPSESSID"]);
$dataSession = $users->checkSession($_COOKIE["PHPSESSID"]);

$GLOBALS['ID'] = $dataSession->id;
$GLOBALS['ID_USUARIO'] = $dataSession->id_usuario;
$GLOBALS['NOMBRE_USUARIO'] = $dataSession->nombre_usuario;
$GLOBALS['TIPO_USUARIO'] = $dataSession->tipo_usuario;

/*
if(http_response_code(404)) {
    $app->get('/', '\MiWeb\Controllers\Welcome\WelcomeController:index')->setName('welcome');
}
*/
if($sessionActive && isset($dataSession->session_id))
{
    $app->get('/home', '\MiWeb\Controllers\Erp\InventarioController:home');
    $app->post('/insert', '\MiWeb\Controllers\Erp\InventarioController:insert');
    $app->post('/update', '\MiWeb\Controllers\Erp\InventarioController:update');
    $app->post('/delete', '\MiWeb\Controllers\Erp\InventarioController:delete');
    $app->get('/logout', '\MiWeb\Controllers\Welcome\WelcomeController:logout')->setName('logout');

}else{
    $app->get('/', '\MiWeb\Controllers\Welcome\WelcomeController:index')->setName('welcome');
    $app->get('/login', '\MiWeb\Controllers\Welcome\WelcomeController:login')->setName('login');
    $app->get('/register', '\MiWeb\Controllers\Welcome\WelcomeController:register')->setName('register');
    $app->post('/loginUser', '\MiWeb\Controllers\Welcome\WelcomeController:loginUser')->setName('loginUser');
    $app->post('/registerUser', '\MiWeb\Controllers\Welcome\WelcomeController:registerUser')->setName('registerUser');
}



